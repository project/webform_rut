## Introduction

This module enable the Rut element for webform.

## Dependencies

* [Rut](https://drupal.org/project/rut)
* [Webform](https://drupal.org/project/webform)
